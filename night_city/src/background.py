"""
background.py
"""

from PIL import ImageDraw

from src.base_entity import BaseEntity


class Background(BaseEntity):
    def __init__(self):
        self.bg_color = (76, 0, 204)
        self.moon_color = (153, 0, 76)
        self.moon_shift = 0

    def move(self, dt):
        pass

    def draw(self, img):
        self._fill_background(img)
        self._draw_moon(img)

    def _fill_background(self, img):
        width, height = img.size
        draw = ImageDraw.Draw(img)
        draw.rectangle(((0, 0), (width, height)), fill=self.bg_color)

    def _draw_moon(self, img):
        width, height = img.size

        moon_widht = width // 10
        moon_height = height // 10

        draw = ImageDraw.Draw(img)
        draw.ellipse(
            ((moon_widht, moon_height + self.moon_shift), (moon_widht + moon_widht, (moon_height + moon_height * 2) + self.moon_shift)), 
            fill=self.moon_color
        )
