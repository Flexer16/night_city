"""
city_gif.py
"""

import os
from PIL import Image

from src.background import Background
from src.layout import Layout


class CityGif:
    def __init__(self):
        self.images = []

        self.width = 1000
        self.height = 300
        self.count = self.height // 2

        self.background = Background()

        self.back_plane = Layout(self.width, self.height, self.count // 2, (102, 0, 102), self.height // 2)
        self.middle_plane = Layout(self.width, self.height, self.count , (102, 0, 51), self.height // 5)
        self.front_plane = Layout(self.width, self.height, self.count * 2, (32, 32, 32), self.height // 10)

    def generate(self):
        for i in range(0, self.count):
            im = Image.new("RGB", (self.width, self.height), (0, 0, 0))
            
            self.background.move(i)
            self.background.draw(im)

            self.back_plane.move(i)
            self.back_plane.draw(im)

            self.middle_plane.move(i)
            self.middle_plane.draw(im)

            self.front_plane.move(i)
            self.front_plane.draw(im)
            
            self.images.append(im)
        
        with open(os.path.join(os.getcwd(), "out/sample.gif"), "wb") as output_file:
            self.images[0].save(output_file, save_all=True, append_images=self.images[1:], optimize=False, duration=40, loop=0)
