"""
layout.py
"""

from random import randint, seed
from datetime import datetime

from PIL import ImageDraw

from src.base_entity import BaseEntity
from src.building import Building


class Layout(BaseEntity):
    def __init__(self, width, height, count, color, base_height):
        seed(datetime.now())

        self.img_widht = width
        self.img_height = height
        self.count = count
        self.base_height = base_height

        self.main_color = color

        self.buildings_list = list()

        for i in range(1, 20):
            rnd_width = randint(self.img_widht // 20, self.img_widht // 10)
            rnd_height = randint(self.img_height // 15, self.img_height // 2)
            rnd_position = randint(0, self.img_widht)
            self.buildings_list.append(
                Building(rnd_width, rnd_height, rnd_position, (self.img_height - self.base_height), self.main_color, (self.count // 30) * 2, self.img_widht)
            )
        
    def move(self, dt):
        for building in self.buildings_list:
            building.move()

    def draw(self, img):
        width, height = img.size
        draw = ImageDraw.Draw(img)
        draw.rectangle(((0, height - self.base_height), (width, height)), fill=self.main_color)
        
        for building in self.buildings_list:
            building.draw(img)
        