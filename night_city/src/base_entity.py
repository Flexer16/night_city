"""
base_entity.py
"""

from abc import ABC, abstractmethod


class BaseEntity(ABC):
    @abstractmethod
    def move(self, dt):
        pass

    @abstractmethod
    def draw(self, img):
        pass
