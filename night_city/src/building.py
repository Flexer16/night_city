"""
bilding.py
"""

from PIL import ImageDraw


class Building:
    def __init__(self, width, height, pos_x, pos_y, color, step, max_width):
        self.width = width
        self.height = height
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.color = color
        self.step = step
        self.max_width = max_width

    def draw(self, img):
        draw = ImageDraw.Draw(img)
        draw.rectangle(((self.pos_x, self.pos_y - self.height), (self.pos_x + self.width, self.pos_y)), fill=self.color)

    def move(self):
        self.pos_x += self.step

        if self.pos_x > self.max_width:
            self.pos_x = 0 - self.width

